package com.netease.mediacapturedemo;

import java.io.FileNotFoundException;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

public class MainActivity extends ActionBarActivity {
	
	private static final int ACTION_TAKE_PHOTO = 1;
	private static final int ACTION_TAKE_VIDEO = 2;
    private static final int ACTION_PICK_PHOTO = 3;
	private static final int ACTION_CAPTURE_SCREEN = 4;
	private static final int ACTION_RECORD_AUDIO = 5;
	
	
	private ImageView mImageView;
	private VideoView mVideoView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mImageView = (ImageView) findViewById(R.id.imageView);
		mVideoView = (VideoView) findViewById(R.id.videoView);
				
		findViewById(R.id.btn_take_photo).setOnClickListener(
		    new OnClickListener(){
				@Override
				public void onClick(View view) {
				    takePhoto();
				}
			});
		
		findViewById(R.id.btn_record_video).setOnClickListener(
			new OnClickListener(){
				@Override
				public void onClick(View view) {
					takeVideo();
				}
			});
		findViewById(R.id.btn_pick_photo).setOnClickListener(
			new OnClickListener(){
				@Override
				public void onClick(View view) {
					pickPhoto();
				}
			});
        findViewById(R.id.btn_capture_screen).setOnClickListener(
            new OnClickListener(){
                @Override
                public void onClick(View view) {
                    mVideoView.setVisibility(View.INVISIBLE);
                    mImageView.setVisibility(View.VISIBLE);
                    mImageView.setImageBitmap(takeScreenshot());
                }
            });
		findViewById(R.id.btn_record_audio).setOnClickListener(
			new OnClickListener(){
				@Override
				public void onClick(View view) {
					Intent intent = new Intent(MainActivity.this, AudioRecordActivity.class);
					MainActivity.this.startActivity(intent);
				}
			});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case ACTION_TAKE_PHOTO:
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                mImageView.setImageBitmap((Bitmap) extras.get("data"));
                mImageView.setVisibility(View.VISIBLE);
                mVideoView.setVisibility(View.INVISIBLE);
            }
            break;
        case ACTION_TAKE_VIDEO:
            if (resultCode == RESULT_OK) {
                mVideoView.setVideoURI(data.getData());
                mVideoView.setVisibility(View.VISIBLE);
                mImageView.setVisibility(View.INVISIBLE);     
            }
            break;
        case ACTION_PICK_PHOTO:
            if (resultCode == RESULT_OK) {
                Uri mImageUri = data.getData();
                if (mImageUri != null) {
                    Bitmap image;
                    try {  
                        //image = MediaStore.Images.Media.getBitmap(
                        //        this.getContentResolver(), mImageUri);
                        image = decodeUri(mImageUri);
                        if (image != null) {
                            mImageView.setImageBitmap(image);  
                        } else {
                            Toast.makeText(this, "image=null", Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }  
                } else {
                    Toast.makeText(this, "uri=null", Toast.LENGTH_LONG).show();
                }
            }
            break;
        case ACTION_CAPTURE_SCREEN:
            break;
        case ACTION_RECORD_AUDIO:
            break;
        default:
            break;
        } 
    }
	
	private void takePhoto() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(takePictureIntent, ACTION_TAKE_PHOTO);
	}
	
	private void takeVideo() {
	    Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
	    startActivityForResult(takeVideoIntent, ACTION_TAKE_VIDEO);
	}
	
	private void pickPhoto() {
        Intent intent = new Intent();
        intent.setType("image/*");
        //intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(intent, ACTION_PICK_PHOTO);
	}
	
	public Bitmap takeScreenshot() {
	    View rootView = findViewById(android.R.id.content).getRootView();
	    rootView.setDrawingCacheEnabled(true);
	    return rootView.getDrawingCache();
	}
	
	private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {

        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(
                getContentResolver().openInputStream(selectedImage), null, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 500;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE && height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(
                getContentResolver().openInputStream(selectedImage), null, o2);

    }
}
